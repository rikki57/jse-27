package ru.nlmk.study.jse27.proxy.var4;

public class StaticModule {
    protected static int counter = 0;

    public static int getCounter() {
        return counter;
    }
}
