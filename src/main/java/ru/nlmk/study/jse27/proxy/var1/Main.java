package ru.nlmk.study.jse27.proxy.var1;

import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        ClassLoader parentClassLoader = BadMagic.class.getClassLoader();
        Class badMagicClass = parentClassLoader.loadClass("ru.nlmk.study.jse27.proxy.var1.BadMagic");
        BadMagic badMagic = (BadMagic) badMagicClass.newInstance();
        badMagic.cast();

        KindMagicClassLoader kindMagicClassLoader = new KindMagicClassLoader(parentClassLoader);
        Class kindMagicClass = kindMagicClassLoader.loadClass("ru.nlmk.study.jse27.proxy.var1.Magic");
        Object magic = kindMagicClass.newInstance();
        kindMagicClass.getMethod("cast").invoke(magic, null);
    }
}
