package ru.nlmk.study.jse27.proxy.var2.javassist;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;
import javassist.util.proxy.ProxyObject;
import ru.nlmk.study.jse27.proxy.var2.dynamic.User;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        User user = new User();

        MethodHandler methodHandler = new MethodHandlerImpl(user);
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setSuperclass(User.class);
        Object instance = proxyFactory.createClass().newInstance();
        ((ProxyObject) instance).setHandler(methodHandler);

        User userProxy = (User)instance;
        System.out.println(userProxy.getName());
    }
}
