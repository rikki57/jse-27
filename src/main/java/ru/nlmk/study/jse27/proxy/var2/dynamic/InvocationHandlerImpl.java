package ru.nlmk.study.jse27.proxy.var2.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class InvocationHandlerImpl implements InvocationHandler {
    private User user;

    public InvocationHandlerImpl(User user) {
        this.user = user;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().equals("getName")){
            return ((String) method.invoke(user, args)).toUpperCase();
        }
        return method.invoke(user, args);
    }
}
