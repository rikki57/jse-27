package ru.nlmk.study.jse27.proxy.var2.dynamic;

public interface IUser {
    String getName();
    Integer getAge();
}
