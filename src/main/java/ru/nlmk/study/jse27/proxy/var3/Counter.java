package ru.nlmk.study.jse27.proxy.var3;

public interface Counter {
    public void count();

    public void read();
}
