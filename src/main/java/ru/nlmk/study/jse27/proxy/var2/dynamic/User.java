package ru.nlmk.study.jse27.proxy.var2.dynamic;

public class User implements IUser{
    private final String name;

    public User() {
        this(null);
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getAge() {
        return 2;
    }
}
