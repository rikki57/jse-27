package ru.nlmk.study.jse27.proxy.var2.dynamic;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Main {
    public static void main(String[] args) {
        User user = new User("Петя");
        InvocationHandler handler = new InvocationHandlerImpl(user);
        IUser proxy = (IUser) Proxy.newProxyInstance(user.getClass().getClassLoader(), User.class.getInterfaces(), handler);
        System.out.println(proxy.getName());
        System.out.println(proxy.getAge());
    }
}
