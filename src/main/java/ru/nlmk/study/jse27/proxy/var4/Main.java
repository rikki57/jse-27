package ru.nlmk.study.jse27.proxy.var4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
        Map<String, Class> classesCache = new HashMap<>();
        ClassLoader loader;
        for(;;){
            loader = new ClassOverloader(classesCache);
            Class clazz = Class.forName("ru.nlmk.study.jse27.proxy.var4.DynamicModule", true, loader);
            StaticModule staticModule;
            staticModule = (StaticModule)clazz.newInstance();
            System.out.println(staticModule.getCounter());
            System.out.println(staticModule);
            new BufferedReader(new InputStreamReader(System.in)).readLine();
        }
    }
}
