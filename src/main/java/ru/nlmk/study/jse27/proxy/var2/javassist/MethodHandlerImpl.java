package ru.nlmk.study.jse27.proxy.var2.javassist;

import javassist.util.proxy.MethodHandler;
import ru.nlmk.study.jse27.proxy.var2.dynamic.User;

import java.lang.reflect.Method;

public class MethodHandlerImpl implements MethodHandler {
    private User user;

    public MethodHandlerImpl(User user) {
        this.user = user;
    }

    @Override
    public Object invoke(Object o, Method method, Method method1, Object[] objects) throws Throwable {
        if(method.getName().equals("getName")){
            return ((String)method.invoke(user, objects)).toUpperCase();
        }
        return method.invoke(user, objects);
    }
}
