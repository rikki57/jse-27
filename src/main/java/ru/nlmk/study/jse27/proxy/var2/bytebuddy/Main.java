package ru.nlmk.study.jse27.proxy.var2.bytebuddy;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.implementation.MethodDelegation;
import ru.nlmk.study.jse27.proxy.var2.dynamic.User;

import static net.bytebuddy.matcher.ElementMatchers.named;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        User user = new User("Петя");
        User userProxy = new ByteBuddy()
                .subclass(User.class)
                .method(named("getName"))
                .intercept(MethodDelegation.to(new MyInterceptor(user)))
                .make()
                .load(User.class.getClassLoader())
                .getLoaded()
                .newInstance();

        System.out.println(userProxy.getName());
    }
}
