package ru.nlmk.study.jse27.proxy.var2.bytebuddy;

import ru.nlmk.study.jse27.proxy.var2.dynamic.User;

public class MyInterceptor {
    User user;

    public MyInterceptor(User user) {
        this.user = user;
    }

    public String getName(){
        return user.getName().toUpperCase();
    }
}
