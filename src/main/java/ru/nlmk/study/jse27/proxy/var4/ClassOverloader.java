package ru.nlmk.study.jse27.proxy.var4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

public class ClassOverloader extends ClassLoader{
    private Map<String, Class> classesCache;

    public ClassOverloader(Map<String, Class> classesCache) {
        this.classesCache = classesCache;
    }

    @Override
    public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
        Class result = findClass(name);
        if (resolve){
            resolveClass(result);
        }
        return result;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        Class result = classesCache.get(name);
        if (result != null){
            System.out.println("Class " + name + " found in cache");
            classesCache.remove(name);
            return result;
        }

        File file = findFile(name, ".class");

        if (file == null){
            return findSystemClass(name);
        } else {
            System.out.println("Class " + name + " found in " + file);
        }

        try{
            byte[] classBytes = loadAsBytes(file);
            result = defineClass(name, classBytes, 0, classBytes.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        classesCache.put(name, result);
        return result;
    }

    private byte[] loadAsBytes(File file) throws FileNotFoundException {
        byte[] result = new byte[(int) file.length()];
        try (FileInputStream f = new FileInputStream(file)) {
            f.read(result, 0, result.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    protected File findFile(String name, String extension){
        File f;
        f = new File(name.substring(name.lastIndexOf(".") + 1) + extension);
        if (f.exists()){
            return f;
        }
        return null;
    }
}
