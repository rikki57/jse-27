package ru.nlmk.study.jse27.proxy.var4;

public class DynamicModule extends StaticModule {
    public String toString(){
        return "Dynamic, version 1! " + (counter++);
    }
}
