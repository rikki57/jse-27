package ru.nlmk.study.jse27.proxy.var2.cglib;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import ru.nlmk.study.jse27.proxy.var2.dynamic.User;

public class Main {
    public static void main(String[] args) {
        User user = new User("Петя");
        MethodInterceptor interceptor = new MethodInterceptorImpl(user);
        User userProxy = (User) Enhancer.create(User.class, interceptor);
        System.out.println(userProxy.getName());
        System.out.println(userProxy.getAge());
    }
}
