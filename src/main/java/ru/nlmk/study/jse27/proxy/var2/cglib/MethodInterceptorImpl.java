package ru.nlmk.study.jse27.proxy.var2.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import ru.nlmk.study.jse27.proxy.var2.dynamic.User;

import java.lang.reflect.Method;

public class MethodInterceptorImpl implements MethodInterceptor {
    private User user;

    public MethodInterceptorImpl(User user) {
        this.user = user;
    }

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        if (method.getName().equals("getName")){
            return ((String)proxy.invoke(user, args)).toUpperCase();
        }
        return proxy.invoke(user, args);
    }
}
