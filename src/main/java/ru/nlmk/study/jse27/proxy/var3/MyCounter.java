package ru.nlmk.study.jse27.proxy.var3;

public class MyCounter implements Counter{
    @Override
    @Logged
    public void count() {
        System.out.println("1-2-3-4");
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        read();
    }

    @Logged
    @Override
    public void read() {
        System.out.println("I am reading");
    }
}
